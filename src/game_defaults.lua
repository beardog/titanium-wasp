
local function initDefaults(ctx)
  ctx.cellSize = 20
  ctx.cellDiscance = 6
  ctx.rowsCount = 22
  ctx.columnsCount = 11
  ctx.windowWidth = ctx.cellSize*ctx.columnsCount + ctx.cellDiscance*(ctx.columnsCount-1)
  ctx.windowHeight = ctx.cellSize*ctx.rowsCount + ctx.cellDiscance*(ctx.rowsCount-1)
  ctx.carHeight = 4
  ctx.carWidth = 3
  -- cell types
  ctx.ctEmpty = 0
  ctx.ctBusy = 1
  -- keys
  ctx.kUp = "up"
  ctx.kDown = "down"
  ctx.kLeft = "left"
  ctx.kRight = "right"
  ctx.kStart = "space" -- starts car moving
  ctx.kRestart = "r" -- restarts game (places everything in start positions)
  ctx.kNone = "none"
  -- speed controls
  ctx.keysUpdateTheshold = 0.5; 
  ctx.updateTheshold = 1.0
  ctx.gearModifiers = {1.0, 0.5, 0.25}
  --ctx.gearModifiers = {3.0, 1.0, 0.5} -- this is very slow, for debug purposes
end

-- ===========================================================================

return {
  init = initDefaults,
}