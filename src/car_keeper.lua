-- car keeper 
-- Maintains and draws player car state
-- letters: ck
-- ===========================================================================

local basic_logger_mobj = require("src/basic_logger")
local lg = {}
basic_logger_mobj.init_logger(lg, "ck")

local defm = require("src/game_defaults")
local rdef = {}
defm.init(rdef)

local ccm = require("src/cells_keeper")

-- ===========================================================================

-- ctx -> car keeper context
local function carKeeperInit(ctx)    
  ctx.baseX = 3
  ctx.baseY = rdef.rowsCount - 1 - rdef.carHeight -- one line over last line
end  

-- ctx -> car keeper context
-- key -> key that was pressed (or rdef.kNone)
-- cells -> cells context or array
local function carKeeperUpdate(ctx, key, cells)
  if (key == rdef.kLeft) then
    local candidatePos = ctx.baseX -1
    if (candidatePos >= 1) then
      ctx.baseX = candidatePos
    end
  elseif(key == rdef.kRight) then
    local candidatePos = ctx.baseX +1
    if (candidatePos <= (rdef.columnsCount - rdef.carWidth +1)) then
      ctx.baseX = candidatePos      
    end
  end 

  ccm.setBusy(cells, ctx.baseX+1, ctx.baseY)
  
  ccm.setBusy(cells, ctx.baseX  , ctx.baseY+1)
  ccm.setBusy(cells, ctx.baseX+1, ctx.baseY+1)
  ccm.setBusy(cells, ctx.baseX+2, ctx.baseY+1)
  
  ccm.setBusy(cells, ctx.baseX+1, ctx.baseY+2)

  ccm.setBusy(cells, ctx.baseX  , ctx.baseY+3)
  ccm.setBusy(cells, ctx.baseX+1, ctx.baseY+3)
  ccm.setBusy(cells, ctx.baseX+2, ctx.baseY+3)
end

-- ===========================================================================

return {
  init = carKeeperInit,
  reset = carKeeperInit, -- note, init is used for both init and reset
  update = carKeeperUpdate,  
}