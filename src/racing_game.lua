local basic_logger_mobj = require("src/basic_logger")
local lg = {}
basic_logger_mobj.init_logger(lg, "rg")

local defm = require("src/game_defaults")
local rdef = {}
defm.init(rdef)

local ekm = require("src/enemy_keeper")
local ckm = require("src/car_keeper")
local ccm = require("src/cells_keeper")

-- ===========================================================================
-- roadblock Keeper ==========================================================
-- ===========================================================================

-- ctx -> roadblock context
local function roadblockKeeperInit(ctx)
  ctx.pattern = {rdef.ctBusy,rdef.ctEmpty,rdef.ctBusy,rdef.ctBusy}
  ctx.currentPos = 1
  ctx.timeAcc = 0
end

-- ctx -> roadblock keeper context
-- diffTime -> update diffTime
-- gear -> current gear
-- cells -> cells keeper
local function roadblockKeeperUpdate(ctx, diffTime, gear, cells)

  if (gear > 0) then
    ctx.timeAcc = ctx.timeAcc + diffTime
    local updateTheshold = rdef.updateTheshold*rdef.gearModifiers[gear]
    if (ctx.timeAcc>= updateTheshold) then    
      ctx.timeAcc = ctx.timeAcc - updateTheshold;

      ctx.currentPos = ctx.currentPos +1
      if (ctx.currentPos > #ctx.pattern) then
        ctx.currentPos = 1
      end  
    end
  end

  local tmpPos = ctx.currentPos
  for i=1, rdef.rowsCount do
    if (ctx.pattern[tmpPos] == rdef.ctBusy) then
      ccm.setBusy(cells, 1, i)
      ccm.setBusy(cells, rdef.columnsCount, i)
    end

    tmpPos = tmpPos +1
    if (tmpPos > #ctx.pattern) then
      tmpPos = 1
    end  
  end

end

local rkm = {}
rkm.init = roadblockKeeperInit
rkm.update = roadblockKeeperUpdate

-- ===========================================================================
-- processing keyboard events ================================================
-- ===========================================================================

local function keysProcessorInit(ctx)
  ctx.timeAcc = 0

  ctx.keysToProcess = { rdef.kLeft, rdef.kRight, rdef.kUp, rdef.kDown,
                        rdef.kStart, rdef.kRestart}
  ctx.pressingCounters = {}

  lg:d("counters:" .. #ctx.keysToProcess);

  for i = 1,#ctx.keysToProcess do
    ctx.pressingCounters[i] = 0
  end    
end

-- ctx -> keysProcessor context
-- diffTime -> love2d update difftime
-- key -> a key string
-- returns: a key string pressed
local function keysProcessorUpdate(ctx, diffTime)

  for i=1,#ctx.keysToProcess do
    if love.keyboard.isDown(ctx.keysToProcess[i]) then
      ctx.pressingCounters[i] = ctx.pressingCounters[i] +1
      break
    end
  end

  ctx.timeAcc = ctx.timeAcc + diffTime

  local resultKey = rdef.kNone
  if (ctx.timeAcc>= rdef.keysUpdateTheshold) then
    ctx.timeAcc = ctx.timeAcc - rdef.keysUpdateTheshold;
     
    local tmpMax = 0;
    local tmpMaxIdx = 0;
    for i=1,#ctx.pressingCounters do
      if (ctx.pressingCounters[i]>tmpMax) then
        tmpMax = ctx.pressingCounters[i];
        tmpMaxIdx = i;
      end
    end
    if (tmpMax >0) then
      resultKey = ctx.keysToProcess[tmpMaxIdx]

      for i=1,#ctx.pressingCounters do
        ctx.pressingCounters[i] = 0;
      end
    end
  end
     
  return resultKey
end

local kpm = {}
kpm.init = keysProcessorInit
kpm.update = keysProcessorUpdate

-- ===========================================================================
-- ===========================================================================
-- ===========================================================================

local function getMessage(ctx)
  if (ctx.gear == 0) then
    if (ctx.cells.hasConflicts) then
      return "Failed :( Press <r> to restart"
    else
      return "Press <space> to start"
    end    
  elseif (ctx.gear == #rdef.gearModifiers) then
    return "Max speed, use <down> to run slower"
  else  
    return "Use <up> to run faster"
  end
end

-- ctx -> racing game context
-- key -> a key that was pressed
local function processGear(ctx, keyPressed)
  if (gear == 0) then
    if (keyPressed == rdef.kStart) then    
      ctx.gear = 1
      lg:d("Car started ")
    else
      lg:d("Ignoring aanything but \"space\"")
    end
    
    return
  end

  if (keyPressed == rdef.kUp) then
    local newGear = ctx.gear +1
    if (newGear <= #rdef.gearModifiers) then
      ctx.gear = newGear
      lg:d("Gear up, to " .. ctx.gear)
    end
    return
  end

  if (keyPressed == rdef.kDown) then
    local newGear = ctx.gear -1
    if (newGear >=1) then -- note gear 0 is not allowed to select
      ctx.gear = newGear
      lg:d("Gear down to " .. ctx.gear)
    end
    return
  end
end

-- ctx -> racing game context
-- diffTime -> time difference as in love.update
local function updateRacingArea(ctx, diffTime)
  local keyPressed = kpm.update(ctx.keysProcessor, diffTime)
  
  if (ctx.cells.hasConflicts) then
    if (keyPressed == rdef.kRestart) then
      ekm.reset(ctx.enemyKeeper)
      ckm.reset(ctx.carKeeper) 
    else
      return
    end
  end
  
  ccm.reset(ctx.cells)  

  processGear(ctx, keyPressed)

  rkm.update(ctx.roadblocksKeeper, diffTime, ctx.gear, ctx.cells)

  ekm.update(ctx.enemyKeeper, diffTime, ctx.gear, ctx.cells)

  ckm.update(ctx.carKeeper, keyPressed, ctx.cells)
  
  if (ctx.cells.hasConflicts) then
    ctx.gear = 0
  end

end

-- ===========================================================================

local getAreaSizes = function(ctx)
  return rdef.windowWidth, rdef.windowHeight
end

-- ===========================================================================

local palette = {}
palette.failedFieldColor = {0.5, 0, 0, 1}
palette.normalFieldColor = {0.8, 0.8, 0.8, 1}
palette.cellColor = {0.3, 0.5, 0.4, 1}
palette.conflictCellColor = {0.8, 0.3, 0.3, 1}

-- ===========================================================================

local drawRacingArea = function(ctx, baseAreaX, baseAreaY)
  if (ctx.cells.hasConflicts) then
    love.graphics.setColor(palette.failedFieldColor)
  else  
    love.graphics.setColor(palette.normalFieldColor)
  end
  local aw, ah = getAreaSizes(ctx)
  love.graphics.rectangle("fill", baseAreaX, baseAreaY, aw, ah)
  
  -- -------  
  for i=1,rdef.rowsCount do
    for j=1,rdef.columnsCount do
      if (ccm.isBusy(ctx.cells, j, i)) then
        local sqcX = j-1;
        local sqX = baseAreaX + sqcX*(rdef.cellSize + rdef.cellDiscance)
  
        local sqcY = i-1;
        local sqY = baseAreaY + sqcY*(rdef.cellSize + rdef.cellDiscance)
  
        if (ccm.isConflict(ctx.cells, j, i)) then
          love.graphics.setColor(palette.conflictCellColor)
        else
          love.graphics.setColor(palette.cellColor)
        end
        love.graphics.rectangle("fill", sqX, sqY, rdef.cellSize, rdef.cellSize)
      end
    end   
  end
end

-- ===========================================================================

local function processCommandLineParams(ctx, args)
  for i=1, #args do
    local seed = string.match(args[i], "%-%-seed=(%d*)")
    if (seed ~= nil) then
      lg:d("Got seed: " .. seed)
      love.math.setRandomSeed( seed )
    end
  end
end


local initRacing = function(ctx, args)
  processCommandLineParams(ctx, args)

  basic_logger_mobj.init_logger(lg, "Racing:");

  ctx.gear = 0 -- TODO: will be replaced with gear keeper
  
  -- roadblocks Keeper
  ctx.roadblocksKeeper = {}
  rkm.init(ctx.roadblocksKeeper)

  -- keys processing
  ctx.keysProcessor = {}
  kpm.init(ctx.keysProcessor)

  -- car keeper
  ctx.carKeeper = {}
  ckm.init(ctx.carKeeper)

  -- enemy keeper
  ctx.enemyKeeper = {}
  ekm.init(ctx.enemyKeeper)

  -- cells keeper
  ctx.cells = {}
  ccm.init(ctx.cells)

end

-- ===========================================================================

return {
  getAreaSizes = getAreaSizes,
  getMessage = getMessage,
  drawRacingArea = drawRacingArea,
  init = initRacing,
  update = updateRacingArea,
  
}