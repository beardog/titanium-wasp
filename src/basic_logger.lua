-- Supposed log levels
-- DBG - printed always to stdout
-- ERR - printed always to stderr

local print_debug_ctx = function(ctx, log_str)
  if (not log_str) then
    return
  end

  if ((ctx == nil) or (ctx.prefix == nil)) then
    print("Undef_log:" .. log_str)
  else
    if (ctx.prefix == "") then
      print(log_str)
    else
      local a,b = math.modf(os.clock())
      if b==0 then 
        b='000' 
      else 
        b=tostring(b):sub(3,5) 
      end

      print(a .. "." .. b .. "::" .. ctx.prefix .. ": " .. log_str)
    end
 end
end

local print_debug_s = function(log_str)
  if (log_str == nil) then
    return
  end

  print(log_str)
end

local print_debug = function(in_1, in_2)
  if ( type(in_1) == "table") then
    print_debug_ctx(in_1, in_2)
  else
    print_debug_s(in_1)
  end
end

local print_error = function(in_1, in_2)
  if ( type(in_1) == "table") then
    print_debug_ctx(in_1, in_2)
  else
    print_debug_s(in_1)
  end
end

local init_logger = function(ctx, prefix_in)
  if (not ctx) then
    print("ERROR: bad 'init_logger' call")
    return
  end
  
  -- prefix setup
  local prefix = "";
  if (prefix_in ~= nil) then
    prefix = prefix_in
  end
  ctx.prefix = prefix

  -- printing functions setup
  ctx.d = print_debug
  ctx.e = print_error
end

return {
  init_logger = init_logger,
}