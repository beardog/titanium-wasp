-- cells/conflicts keeper context
-- Maintains cells array and detects conflicts
-- letters: cc
-- ===========================================================================

local basic_logger_mobj = require("src/basic_logger")
local lg = {}
basic_logger_mobj.init_logger(lg, "cc")

local defm = require("src/game_defaults")
local rdef = {}
defm.init(rdef)

-- ===========================================================================

local function resetCells(ctx)
  ctx.hasConflicts = false
  
  for i=1,rdef.rowsCount do
    ctx.cells[i] = {}
    ctx.conflicts[i] = {}
    for j=1,rdef.columnsCount do
      ctx.cells[i][j] = rdef.ctEmpty
      ctx.conflicts[i][j] = rdef.ctEmpty
    end
  end
end

-- ===========================================================================

local function initCells(ctx)
  ctx.cells = {}
  ctx.conflicts = {}
  ctx.hasConflicts = false
  resetCells(ctx)
end

local function setBusy(ctx, x,y)
  if ((x<1) or (x>rdef.columnsCount)) then
    return    
  end

  if ((y<1) or (y>rdef.rowsCount)) then
    return
  end

  if (ctx.cells[y][x] == rdef.ctBusy) then
    ctx.conflicts[y][x] = rdef.ctBusy
    ctx.hasConflicts = true
  end

  ctx.cells[y][x] = rdef.ctBusy
end

local function isBusy(ctx, x,y)
  if (ctx.cells[y][x] == rdef.ctBusy) then
    return true
  end  

  return false
end

local function isConflict(ctx, x,y)
  if (ctx.conflicts[y][x] == rdef.ctBusy) then
    return true
  end  

  return false
end

-- ===========================================================================

return {
  init = initCells,
  reset = resetCells,
  setBusy = setBusy,
  isBusy = isBusy,
  isConflict = isConflict
}