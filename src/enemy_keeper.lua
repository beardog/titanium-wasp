-- enemy keeper context
-- "enemy" is supposed to be any obstacle on the road
-- ===========================================================================

local basic_logger_mobj = require("src/basic_logger")
local lg = {}
basic_logger_mobj.init_logger(lg, "ek")

local defm = require("src/game_defaults")
local rdef = {}
defm.init(rdef)

local ccm = require("src/cells_keeper")

local posType = {}
posType.left = 1
posType.right = 2
posType.empty = 3

-- ===========================================================================

-- ctx -> enemy keeper context
local function singleKeeperInit(ctx)
  ctx.height = 3
  ctx.baseX = 7
  ctx.baseY = 0 - ctx.height +1
  
  ctx.valid = false
end  

local function prepareNewItem(ctx, prevCtx)
  local newItemType = love.math.random( 2 )
  if (newItemType ==1) then
    ctx.baseX = 3
    lg:d("New item is left")
  else
    lg:d("New item is right")
    ctx.baseX = 7
  end
  
  ctx.height = 3
  local emptyAreaBonus = 1
  if (ctx.baseX ~= prevCtx.baseX) then
    emptyAreaBonus = 5
    lg:d("Empty area added")
  end
  
  ctx.baseY = 1 - ctx.height - emptyAreaBonus 
  if (prevCtx.baseY <=0) then
    ctx.baseY = prevCtx.baseY - ctx.height - emptyAreaBonus 
  end 
  lg:d("Placed at:" .. ctx.baseY .. " prev now is at " .. prevCtx.baseY)
    
  ctx.valid = true
end

-- ===========================================================================

local function drawOneItem(ctx, cells)
  local x = ctx.baseX
  local y = ctx.baseY

  if (y>rdef.rowsCount) then
    --lg:d("item is out")
    ctx.valid = false
    return
  end

  if (y>=1) then
    ccm.setBusy(cells, x+1, y)
  end

  y = y +1
  if (y>rdef.rowsCount) then
    return
  end

  if (y>=1) then
    ccm.setBusy(cells, x, y)
    ccm.setBusy(cells, x+1, y)
    ccm.setBusy(cells, x+2, y)    
  end

  y = y +1
  if (y>rdef.rowsCount) then
    return
  end

  if (y>=1) then
    ccm.setBusy(cells, x, y)
    ccm.setBusy(cells, x+2, y)
  end
end

-- ===========================================================================

local function prepareItemsForGameStart(ctx)
  local startYPositions = {18, 12, 7, 2, -3}
  
  if (#startYPositions > #ctx.items) then
    lg:e("Bad setup:" .. #startYPositions .. " vs " .. #ctx.items)
  end
  
  for i=1, #startYPositions do
    ctx.items[i].valid = true
    ctx.items[i].baseY = startYPositions[i]
    ctx.items[i].baseX = 7
  end
end

local function enemyKeeperInit(ctx)
  ctx.items = { {}, {}, {}, {} , {}} -- so far there is a fixed amount of enemies
  for i=1, #ctx.items do
    singleKeeperInit(ctx.items[i])
  end
  
  prepareItemsForGameStart(ctx)

  ctx.timeAcc = 0
end

local function enemyKeeperReset(ctx)
  prepareItemsForGameStart(ctx)

  ctx.timeAcc = 0
end

-- ===========================================================================

local 
function enemyKeeperUpdate(ctx, diffTime, gear, cells)
  if (gear > 0) then
    ctx.timeAcc = ctx.timeAcc + diffTime
    local updateTheshold = rdef.updateTheshold*rdef.gearModifiers[gear]
    if (ctx.timeAcc>= updateTheshold) then
      ctx.timeAcc = ctx.timeAcc - updateTheshold;

      for i=1, #ctx.items do
        if (ctx.items[i].valid) then
          ctx.items[i].baseY = ctx.items[i].baseY +1
        end
      end  
    end
  end
  
  local prevItemIdx = #ctx.items
  for i=1, #ctx.items do
    --local needRegen = 
    drawOneItem(ctx.items[i], cells)    
    if (false == ctx.items[i].valid) then
      prepareNewItem(ctx.items[i], ctx.items[prevItemIdx])
      ctx.items[i].valid = true
      lg:d("New item generated for " .. i .. " slot")
    end
    prevItemIdx = i
  end 

end

-- ===========================================================================

return {
  init = enemyKeeperInit,
  reset = enemyKeeperReset,
  update = enemyKeeperUpdate,
}