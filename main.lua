
local basic_logger_mobj = require("src/basic_logger")
local rgm = require("src/racing_game")

-- ===========================================================================

local lg = {}

local rg -- racing game context

local gws = {} -- game window settings, similar to "rdef" in other files
gws.windowWidthExt = 40
gws.windowHeightExt = 80 
gws.gameAreaX = 0
gws.gameAreaY = gws.windowHeightExt 


gws.borderLineWidth = 4
gws.gameAreaWidth = 0 -- proper value will appear after init
gws.gameAreaHeight = 0 -- proper value will appear after init 

-- ===========================================================================
-- ===========================================================================

function love.load(arg)  

  basic_logger_mobj.init_logger(lg, "Main");

  -- -------
  rg = {}
  rgm.init(rg, arg)

  -- -------
  gws.gameAreaWidth, gws.gameAreaHeight = rgm.getAreaSizes()
  local screenWidth = gws.gameAreaWidth + gws.windowWidthExt
  local screenHeight = gws.gameAreaHeight + gws.windowHeightExt

  local success = love.window.setMode(screenWidth, screenHeight, 
                                      {resizable=false, minwidth=100, minheight=300})
  if (not success) then
    lg:e("Failed to set window mode")
  end

end

function love.update(diffTime)
    -- TODO: check result here
    rgm.update(rg, diffTime)
 
    if love.keyboard.isDown("escape") then
        lg:d("Exiting after escape pressed")
        love.event.quit( 0 )
    end
end

-- following lines are for debug purposes
--function love.keypressed(key)
--    print(key)
--end

local boardColors = {}
boardColors.emptyScreen = {0.5, 0.5, 0.8, 1} -- 
boardColors.gameAreaBorder = {0.1, 0.1, 0.1, 1} -- 

function love.draw()
  local cww = love.graphics.getWidth(); -- current window width
  local cwh = love.graphics.getHeight();-- current window height 

  love.graphics.setColor(boardColors.emptyScreen)
  love.graphics.rectangle("fill", 0, 0, cww, cwh)

  love.graphics.setColor(boardColors.gameAreaBorder)
  love.graphics.rectangle("fill", 0, gws.gameAreaY-gws.borderLineWidth, 
                          gws.borderLineWidth+gws.gameAreaWidth, gws.borderLineWidth)
  love.graphics.rectangle("fill", gws.gameAreaWidth, gws.gameAreaY-gws.borderLineWidth, 
                          gws.borderLineWidth, gws.borderLineWidth+gws.gameAreaHeight)
  local rgMessage = rgm.getMessage(rg)
  if (rgMessage) then
    love.graphics.print(rgMessage , 5, 10)
  end
  
  -- ---- ----
  rgm.drawRacingArea(rg, gws.gameAreaX, gws.gameAreaY)
end
